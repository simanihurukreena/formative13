SELECT pro.name AS Product, ma.name AS Manufacturer,
br.name AS Brand, addrs.street AS Address, 
val.code AS Valuta, pr.amount AS Harga
FROM price pr
LEFT JOIN product pro ON pro.id = pr.productID
LEFT JOIN brand br ON br.id = pro.brandID
LEFT JOIN manufacturer ma ON ma.id = pro.manufactureID
LEFT JOIN address addrs ON addrs.id = ma.addressID
LEFT JOIN valuta val ON val.id = pr.valutaID
WHERE val.id = 1
ORDER BY Harga;