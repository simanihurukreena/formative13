CREATE TABLE address (
	id INT NOT NULL AUTO_INCREMENT,
    street VARCHAR(50),
    city VARCHAR(30),
    provinces VARCHAR(30),
    country VARCHAR(30),
    zipCode INT,
    PRIMARY KEY(id)
);

CREATE TABLE manufacturer (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    addressID INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(addressID) REFERENCES address(id)
);

CREATE TABLE brand (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    PRIMARY KEY(id)
);

CREATE TABLE product (
	id INT NOT NULL AUTO_INCREMENT,
    artNumber VARCHAR(10),
    name VARCHAR(50),
    description TEXT,
    manufactureID INT,
    brandID INT,
    stock INT,
    PRIMARY KEY(id),
    FOREIGN KEY(manufactureID) REFERENCES manufacturer(id),
    FOREIGN KEY(brandID) REFERENCES brand(id)
);

CREATE TABLE price (
	id INT NOT NULL AUTO_INCREMENT,
    productID INT,
    valutaID INT,
    amount DECIMAL,
    PRIMARY KEY(id),
    FOREIGN KEY(productID) REFERENCES product(id),
    FOREIGN KEY(valutaID) REFERENCES valuta(id)
);

CREATE TABLE valuta (
	id INT NOT NULL AUTO_INCREMENT,
    code VARCHAR(5),
    name VARCHAR(50),
    PRIMARY KEY(id)
);

show tables;