INSERT INTO product(artNumber, name, description, manufactureID, brandID, stock) VALUES
('a012', 'Kopi susu', 'kopi susu merupakan espresso campur susu', 1, 1, 0), ('a017', 'Kopi Gul Aren', 'Kopi gul aren merupakan kopi susu campur gula aren', 1, 2, 0), 
('a013', 'Wings', 'Wings merupakan sayap ayam', 2, 1, 2), ('a018', 'Kue nanas', 'Kue nanas adalah kue nanas', 2, 2, 3),
('a014', 'Pepsi', 'Pepsi adalah minuman bersoda', 1, 1, 5), ('a019', 'Berdisusu', 'Berdisusu adalah susu campur yogurt', 1, 2, 15),
('a015', 'Soup Jagung', 'Soup jagung adalah sop', 2, 1, 10), ('a020', 'BlackMonges', 'BlackMoonges adalah kopi susu skm', 2, 2, 30),
('a016', 'Melon Ice', 'Melon Ice adalah melon with ice cube', 1, 2, 8), ('a021', 'EspressoDS', 'EspressoDS adalahan esspresso double shoot', 1, 2, 20);

INSERT INTO manufacturer(name, addressID) VALUES ('Natural Rubber', 1), ('Belgium', 2);

INSERT INTO brand(name) VALUES ('kfc'), ('aqua');

INSERT INTO address(street, city, provinces, country, zipCode) VALUES
('Jl.Paingan II', 'Sleman', 'YK', 'YK', 34562 ) , ('Jl. Perumnas', 'Bengkulu', 'BKL', 'BKL', 23521);

INSERT INTO price(productID, valutaID, amount) VALUES
( 21, 1, 15000), ( 21, 2, 1.06), 
( 22, 1, 20000), ( 22, 2, 1.42), 
( 23, 1, 25000), ( 23, 2, 1.77), 
( 24, 1, 25000), ( 24, 2, 1.77), 
( 25, 1, 15000), ( 25, 2, 1.06), 
( 26, 1, 20000), ( 26, 2, 1.42), 
( 27, 1, 30000), ( 27, 2, 2.13), 
( 28, 1, 20000), ( 28, 2, 1.42), 
( 29, 1, 15000), ( 29, 2, 1.06), 
( 30, 1, 30000), ( 30, 2, 2.13);

INSERT INTO valuta(code, name) VALUES 
('IDR', 'Rupiah'), ( 'USD', 'US Dollar');


